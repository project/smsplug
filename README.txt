
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Requirements
 * Installation & Upgrading
 * Settings
 * Maintainers


INTRODUCTION
------------

The SMSPlug module allows users to send sms from their community site. The administrator
can define, how many sms each user can send per month.

FEATURES
--------

 * Interacts with Profile module.
 * Logs information per each user.

REQUIREMENTS
------------

The SMSPlug module requires the Profile module which is a Core module that
comes with Drupal. It requires you to make a field of the type 'textfield' and 
with the name 'profile_handynumber'. 


INSTALLATION & UPGRADING
------------------------

See INSTALL.txt for install/upgrade instructions.


SETTINGS
--------

Setting the module is done on a number of pages.

  SMSPlug administration pages
  ------------------------------
  The SMSPlug administration pages (?q=admin/settings/smsplug) is where the
  actual features of the SMSPlug module are set. Some of the options are
  described below.
  * 'Number of SMS per month': here you can select how many sms each user can send
  * 'Originator': the default is the domain name of the drupal installation, this 
    is displayed instead of a number
  * 'Userkey': this key is provided by aspsms.com
  * 'Password': this is the password which you use to access aspsms.com
  * 'SMSPlug log settings': here you can define what should be logged.

  Permissions
  -----------
  To allow users to view SMSPlug, the 'access smsplug'
  permission needs to be set for the appropiate role. This allows them to
  see activated blocks with SMSPlug form.


MAINTAINERS
-----------

 * Marco Bischoff (marco_b) - http://drupal.org/user/491194
