<?php
  class SMS 
  {
   	var $userkey;
    var $password;
    var $originator;
    var $recipients;
    var $content;
    var $obj;
    
    function SMS($u, $p)
  	{
    	$this->userkey = $u;
    	$this->password = $p;
    	$this->recipients = array();
    	$this->blinking = false;
    	$this->flashing = false;
    	$this->originator = "";
    	$this->debug = 0;
    	$this->timeout = 2;
    	$this->notification = array();
    	$this->obj = array();
    	$this->content = ""; 
    	$this->servers = array( "xml1.aspsms.com:5061",
			    "xml1.aspsms.com:5098",
			    "xml2.aspsms.com:5061",
			    "xml2.aspsms.com:5098" );
  	}
  	
  	function setResponse($action) 
  	{
  		$msg = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n".
		   "<aspsms>\r\n".
		   "  <Userkey>".$this->userkey."</Userkey>\r\n".
		   "  <Password>".$this->password."</Password>\r\n".
		   "  <AffiliateId>128407</AffiliateId>\r\n".
		    $action.
		   "</aspsms>\n";
	 	
	 	$fp = fsockopen('xml1.aspsms.com',5061);
      	if ($fp) 
      	{
        	fputs($fp, "POST /xmlsvr.asp HTTP/1.0\r\n");
        	fputs($fp, "Content-Type: text/xml\r\n");
        	fputs($fp, "Content-Length: ".strlen($msg)."\r\n");
        	fputs($fp, "\r\n");
        	fputs($fp, $msg);

        	$content = 0;
        	$reply = array();
        	while (!feof($fp)) 
        	{
				$reply[] = fgets( $fp, 1024 );
      	 	}
		}
		
		fclose($fp);
		
		return $reply; 
  	}
  	
  	function showCredits() 
  	{
  		$result = $this->setResponse("<Action>ShowCredits</Action>\r\n");
  		$result = $this->parse_data($result, "Credits");
  		
  		return $result;
  	}
  	
  	function setOriginator($originator)
  	{
  		$this->originator = $originator;
  	}
  	
  	function sendSMS()
  	{
  		$originator = "<Originator>".$this->originator."</Originator>\r\n";
  		$recipient = "";
  		foreach($this->recipients as $numbers) 
  		{
  			$recipient .= "  <Recipient>\r\n".
				 	 "    <PhoneNumber>".$numbers['number']."</PhoneNumber>\r\n".
				 	 "  </Recipient>\r\n";
		}
		
		$content = $this->content;
  		
  		$result = $this->setResponse($originator.$recipient.$content.'<Action>SendTextSMS</Action>');
  		$result = $this->parse_data($result, "ErrorDescription");
  		
		return $result;
  	}
  	
  	function setContent($msg) 
  	{
  		$this->content = "<MessageData>".htmlspecialchars($msg)."</MessageData>";
  	}
  	
  	function addRecipient($number)
    {
   		$this->recipients[] = array("number" => $number);
  	}
  	
	function getRecipient()
	{
		$recipient = "";
		foreach($this->recipients as $numbers) 
  		{
  			$recipient .= "  <Recipient>\r\n".
				 	 "    <PhoneNumber>".$numbers['number']."</PhoneNumber>\r\n".
				 	 "  </Recipient>\r\n";
		}
		return $recipient;
	}
	
  	function parse_data($result, $action)
  	{
  		$result = $result[11];
  		$xml = simplexml_load_string($result);
  		
  		if($xml === false)
  			$this->obj['error'] = "Error while converting XML";
  			$this->obj['data'] = $xml->$action;
  			  		
  		return $this->obj;
  	}
  }
?>
